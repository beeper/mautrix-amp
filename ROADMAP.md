# Features & roadmap

* Matrix → Android Messages
  * [ ] Message content
    * [x] Text
    * [ ] Media
* Android Messages → Matrix
  * [ ] Message content
    * [x] Text
    * [ ] Media
  * [x] Message history
    * [x] When creating portal
    * [x] Missed messages
* Misc
  * [ ] Automatic portal creation
    * [x] At startup
    * [ ] When receiving invite or message
  * [ ] Provisioning API for logging in
  * [x] Use own Matrix account for messages sent from Android Messages app
  * [x] E2EE in Matrix rooms
